const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const requestValidate = async (req, res, next) => {
  try {
    let friendRequestDAO = new DAO("friendRequest");
    let friendPendingDAO = new DAO("friendPending");
    let friendDAO = new DAO("friends");

    let { userRequestId } = req.body;

    if (userRequestId.toString() === req.userId.toString())
      return returnToUser.errorWithMess(
        res,
        "Không được gửi lời mời kết bạn với chính bản thân"
      );

    let request = await friendRequestDAO.getOne({
      userId: req.userId,
      "friendRequests.userRequestId": userRequestId,
    });
    if (request.success)
      return returnToUser.errorWithMess(res, "Đã gửi lời mời đến người này");

    let friend = await friendDAO.getOne({
      userId: req.userId,
      "friends.friendId": userRequestId,
    });

    if (friend.success) return returnToUser.errorWithMess(res, "Đã là bạn bè");

    let pending = await friendPendingDAO.getOne({
      userId: userRequestId,
      "friendPending.userPendingId": req.userId,
    });
    if (pending.success)
      return returnToUser.errorWithMess(
        res,
        "Bạn đã nhận được một lời mời kết bạn từ người này"
      );
    next();
  } catch (error) {
    console.log(error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = requestValidate;
