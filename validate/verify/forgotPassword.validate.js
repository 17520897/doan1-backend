const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const forgotPasswordValidate = async (req, res, next) => {
  try {
    //define
    let verifyDAO = new DAO("verify");
    let { verifyCode } = req.body;
    //get verify data
    let verify = await verifyDAO.getOne({ verifyCode });
    if (!verify.success || verify.payload.verifyType !== "ForgotPassword") {
      return returnToUser.errorWithMess(
        res,
        "Mã xác thực không tồn tại hoặc đã hết hạn"
      );
    }
    req.userId = verify.payload.verifyUserId;
    req.verifyId = verify.payload._id;
    next();
  } catch (error) {
    console.log("Validate verify expire: ", error);
    returnToUser.errorWithMess(
      res,
      "Xảy ra lỗi trong quá trình xác thực vui lòng xác thực lại"
    );
  }
};

module.exports = forgotPasswordValidate;
