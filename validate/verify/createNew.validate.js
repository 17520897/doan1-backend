const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const mail = require("../../services/mail");

const createNewValidate = async (req, res, next) => {
  try {
    let usersDAO = new DAO("users");
    let { userEmail, verifyType } = req.body;
    let user = await usersDAO.getOne({ userEmail });
    if (!user.success)
      return returnToUser.errorWithMess(res, "người dùng không tồn tại");
    if (verifyType === "Register" && user.payload.isActive) {
      return returnToUser.errorWithMess(res, "Tài khoản đã được kích hoạt");
    }
    req.verify = {
      userId: user.payload._id,
      verifyType,
      userEmail,
    };
    next();
  } catch (error) {
    console.log("create new validate", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = createNewValidate;
