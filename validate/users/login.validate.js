const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const bcrypt = require("bcryptjs");

const loginValidate = async (req, res, next) => {
  try {
    let usersDAO = new DAO("users");
    let { userEmail, userPassword } = req.body;
    let user = await usersDAO.getOne({ userEmail });
    if (!user.success)
      return returnToUser.errorWithMess(res, "Tài khoản không tồn tại");
    if (!(await bcrypt.compare(userPassword, user.payload.userPassword)))
      return returnToUser.errorWithMess(res, "Sai mật khẩu");
    if (!user.payload.isActive)
      return returnToUser.errorWithMess(res, "Tài khoản chưa được kích hoạt");
    req.userId = user.payload._id;
    next();
  } catch (error) {
    console.log("loginvalidate: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = loginValidate;
