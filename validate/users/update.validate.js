const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const bcrypt = require("bcryptjs");
const updateValidate = async (req, res, next) => {
  try {
    if (!req.body.oldPassword) return next();
    let usersDAO = new DAO("users");
    let user = await usersDAO.getById(req.userId);
    if (!user.success)
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi khi cập nhật người dung"
      );
    if (
      !(await bcrypt.compare(req.body.oldPassword, user.payload.userPassword))
    )
      return returnToUser.errorWithMess(res, "Sai mật khẩu cũ");
    req.body.userPassword = await bcrypt.hash(req.body.userPassword, 10);
    next();
  } catch (error) {
    console.log("update validate user: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = updateValidate;
