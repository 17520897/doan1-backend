const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const registerValidate = async (req, res, next) => {
  try {
    let usersDAO = new DAO("users");
    let { userEmail } = req.body;
    let users = await usersDAO.getOne({ userEmail });
    if (users.success) {
      return returnToUser.errorWithMess(res, "Email đã được đăng kí");
    }
    next();
  } catch (error) {
    console.log("register validate", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = registerValidate;
