const express = require("express");
const router = express.Router();

const uploadImagesController = require("../../../controllers/files/uploadImages.controller");
const uploadFileController = require("../../../controllers/files/uploadFiles.controller");
const { uploadFile } = require("../../../services/uploadFile");

router.post(
  "/image",
  uploadFile(["jpg", "png", "jpeg"]).single("file"),
  uploadImagesController
);

router.post("/", uploadFile().single("file"), uploadFileController);

module.exports = router;
