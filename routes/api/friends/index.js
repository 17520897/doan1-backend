const express = require("express");
const router = express.Router();

const sendFriendRequestController = require("../../../controllers/friendRequest/sendFriendRequest.controller");
const getFriendRequestController = require("../../../controllers/friendRequest/getFriendRequest.controller");
const getFriendPendingController = require("../../../controllers/friendPending/getFriendPending.controller");
const acceptFriendPendingController = require("../../../controllers/friends/acceptFriendPending.controller");
const getFriendController = require("../../../controllers/friends/getFriend.controller");
const denyFriendPendingController = require("../../../controllers/friends/denyFriendPending.controller");
const deleteFriendRequestController = require("../../../controllers/friendRequest/deleteFriendRequest.controller");
const requestValidate = require("../../../validate/friends/request.validate");

router.post("/request", requestValidate, sendFriendRequestController);
router.post("/accept", acceptFriendPendingController);
router.post("/deny", denyFriendPendingController);
router.get("/request", getFriendRequestController);
router.get("/pending", getFriendPendingController);
router.get("/", getFriendController);
router.delete("/request", deleteFriendRequestController);
module.exports = router;
