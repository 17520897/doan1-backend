var express = require("express");
var router = express.Router();

const usersValidate = require("../../validate/users/users.validate");

router.use("/files", require("./files"));

router.use("/users", require("./users"));
router.use("/verify", require("./verify"));
router.use("/rooms", require("./rooms"));
router.use("/music", require("./musics"));
router.use(usersValidate);
router.use("/friends", require("./friends"));
router.use("/notify", require("./notify"));
router.use("/roomList", require("./userRooms"));

module.exports = router;
