const express = require("express");
const router = express.Router();

const getUsersNotifyController = require("../../../controllers/notify/getUsersNotify.controller");
const seenNotifyController = require("../../../controllers/notify/seenNotify.controller");
router.get("/", getUsersNotifyController);
router.get("/seen", seenNotifyController);
module.exports = router;
