const express = require("express");
const router = express.Router();

const getAllMusicsController = require("../../../controllers/musics/getAllMusics.controller");
router.get("/", getAllMusicsController);
module.exports = router;
