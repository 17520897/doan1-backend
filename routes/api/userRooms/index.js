const express = require("express");
const router = express.Router();

const getUserRoomsController = require("../../../controllers/userRooms/getUserRooms.controller");

router.get("/", getUserRoomsController);

module.exports = router;
