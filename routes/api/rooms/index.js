const express = require("express");
const router = express.Router();

const getRoomMessageController = require("../../../controllers/rooms/getRoomMessages.controller");
const getRoomImagesController = require("../../../controllers/rooms/getRoomImages.controller");
const seenRoomMessageController = require("../../../controllers/rooms/seenRoomMessage.controller");
router.get("/messages", getRoomMessageController);
router.get("/messages/seen", seenRoomMessageController);
router.get("/images", getRoomImagesController);
module.exports = router;
