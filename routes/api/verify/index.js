const express = require("express");
const router = express.Router();

const verifyUsersController = require("../../../controllers/verify/verifyRegisterUsers.controller");
const createNewVerifyController = require("../../../controllers/verify/createNewVerify.controller");
const verifyForgotPasswordController = require("../../../controllers/verify/verifyForgotPassword.controller");

const verifyValidate = require("../../../validate/verify/register.validate");
const createNewValidate = require("../../../validate/verify/createNew.validate");
const forgotPasswordValidate = require("../../../validate/verify/forgotPassword.validate");

router.post("/register", verifyValidate, verifyUsersController);
router.post(
  "/forgotPassword",
  forgotPasswordValidate,
  verifyForgotPasswordController
);
router.post("/", createNewValidate, createNewVerifyController);

module.exports = router;
