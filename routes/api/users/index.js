const express = require("express");
const router = express.Router();

const registerUsersController = require("../../../controllers/users/registerUsers.controller");
const loginUsersController = require("../../../controllers/users/loginUsers.controller");
const getUsersController = require("../../../controllers/users/getUsers.controller");
const updateUsersController = require("../../../controllers/users/updateUsers.controller");
const seachUsersController = require("../../../controllers/users/searchUsers.controller");

const registerValidate = require("../../../validate/users/register.validate");
const loginValidate = require("../../../validate/users/login.validate");
const usersValidate = require("../../../validate/users/users.validate");
const updateValidate = require("../../../validate/users/update.validate");

router.post("/register", registerValidate, registerUsersController);
router.post("/login", loginValidate, loginUsersController);
router.use(usersValidate);
router.get("/", getUsersController);
router.post("/search", seachUsersController);
router.put("/", updateValidate, updateUsersController);

module.exports = router;
