var express = require("express");
var router = express.Router();
let DAO = require("../classes/DAO");
let jsonwebtoken = require("jsonwebtoken");
router.get("/", async (req, res) => {
  res.send("nothing to see");
});

router.get("/test", async (req, res) => {
  try {
    const io = req.io;
    let userRoomsDAO = new DAO("userRooms");
    const room = await userRoomsDAO.getOne({});
    console.log(io);
    res.send("anc");
  } catch (err) {
    console.log(err);
  }
});

router.use("/api", require("./api"));

module.exports = router;
