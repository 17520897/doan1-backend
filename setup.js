const DAO = require("./classes/DAO");
const bcrypt = require("bcryptjs");
const fs = require("async-file");
require("dotenv").config();
require("./model/connect");
require("./model/schema");

const setup = async () => {
  let configDAO = new DAO("config");
  let config = await configDAO.getOne({});
  if (!config.success) {
    config = await configDAO.create({
      configRegisterEmailTemplate: `
      <html>
       Mã xác nhận kích hoạt tài khoản của bạn là:  <b>{code}</b><br>
       <b style='color: red;'>Lưu ý: </b> mã xác nhận chỉ có giá trị trong 30 phút
      </html>
    `,
      configVerifyEmailTemplate: `
    <html>
    Mã xác nhận quên mật khẩu của bạn là:  <b>{code}</b>
    <br>
       <b style='color: red;'>Lưu ý: </b> mã xác nhận chỉ có giá trị trong 30 phút
    </html>
  `,
      configNotifyEmailTemplate: `
    <html>
      <b>Mã xác thực trang web của bạn là: {code}</b>
    </html>
  `,
    });
    if (config.success) console.log("success");
    else console.log("error");
  }
  const uploadsDir = await fs.exists("public/uploads");
  if (!uploadsDir) await fs.mkdir("public/uploads");
  console.log("make uploads folder success");
  const musicDAO = new DAO("musics");
  await musicDAO.create({
    name: "Chúng ta không thuộc về nhau",
    url: `${process.env.SERVER_URL}/music/ChungTaKhongThuocVeNhau.mp3`,
    singer: `Sơn Tùng MTP`,
  });
  await musicDAO.create({
    name: "Là một thằng con trai",
    url: `${process.env.SERVER_URL}/music/LaMotThangConTrai.mp3`,
    singer: `Jack`,
  });
  await musicDAO.create({
    name: "Một cú lừa",
    url: `${process.env.SERVER_URL}/music/MotCuLua.mp3`,
    singer: `Bích Phương`,
  });
  await musicDAO.create({
    name: "Đường một chiều",
    url: `${process.env.SERVER_URL}/music/DuongMotChieu.mp3`,
    singer: `Huỳnh Tú`,
  });
  await musicDAO.create({
    name: "Em bỏ thuốc chưa",
    url: `${process.env.SERVER_URL}/music/EmBoThuocChua.mp3`,
    singer: `Bích Phương`,
  });
  await musicDAO.create({
    name: "Em của ngày hôm qua",
    url: `${process.env.SERVER_URL}/music/EmCuaNgayHomQua.mp3`,
    singer: `Sơn Tùng MTP`,
  });
  await musicDAO.create({
    name: "Hoa nở không màu",
    url: `${process.env.SERVER_URL}/music/HoaNoKhongMau.mp3`,
    singer: `Hoài Lâm`,
  });
  await musicDAO.create({
    name: "Không phải dạng vừa đâu",
    url: `${process.env.SERVER_URL}/music/KhongPhaiDangVuaDau.mp3`,
    singer: `Sơn Tùng MTP`,
  });
  await musicDAO.create({
    name: "Từ chối nhẹ nhàng thôi",
    url: `${process.env.SERVER_URL}/music/TuChoiNheNhangThoi.mp3`,
    singer: `Bích Phương`,
  });
  console.log("setup music success");
};

setup();
