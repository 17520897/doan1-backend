function openStream() {
  const config = { audio: false, video: true };
  return navigator.mediaDevices.getUserMedia(config);
}

function playStream(idVideoTag, stream) {
  const video = document.getElementById(idVideoTag);
  video.srcObject = stream;
  video.play();
}
//openStream().then((stream) => playStream("localStream", stream));
const socket = io("http://localhost:3540");

const peer = new Peer({
  host: "localhost",
  port: 3541,
  path: "/webRtc",
});

peer.on("open", (id) => $("#yourId").append(id));

peer.on("disconnected", (data) => {
  console.log("dis");
});

peer.on("close", (data) => {
  console.log("close");
});

//Caller

$("#btnCall").click(() => {
  const remoteId = $("#remoteId").val();
  openStream().then((stream) => {
    const call = peer.call(remoteId, stream);
    call.on("stream", (remoteStream) => {
      playStream("localStream", stream);
      playStream("remoteStream", remoteStream);
      peer.on("connection", (data) => {
        console.log(data);
      });
      $("#btnCancel").click(() => {
        let audioTrack = stream.getAudioTracks();
        console.log(audioTrack);
        if (audioTrack.length > 0) stream.removeTrack(audioTrack[0]);
        audioTrack = remoteStream.getAudioTracks();
        if (audioTrack.length > 0) remoteStream.removeTrack(audioTrack[0]);
        call.close();
      });
    });
  });
});

//
peer.on("call", (call) => {
  $("#btnAnswer").click(() => {
    socket.emit("Client-send");
    openStream().then((stream) => {
      call.answer(stream);
      call.on("stream", (remoteStream) => {
        playStream("localStream", stream);
        playStream("remoteStream", remoteStream);
        $("#btnCancel").click(() => {
          let audioTrack = stream.getAudioTracks();
          console.log(audioTrack);
          if (audioTrack.length > 0) stream.removeTrack(audioTrack[0]);
          audioTrack = remoteStream.getAudioTracks();
          if (audioTrack.length > 0) remoteStream.removeTrack(audioTrack[0]);
          call.close();
        });
      });
    });
  });
});
