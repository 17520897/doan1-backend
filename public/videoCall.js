const serverUrl = "backend.pdchatapp.tk";
const socket = io(`${serverUrl}`);
let peer;
let userId;
let answerSocketId;
let answerWebRtcId;
let socketId;
let isCaller = false;
let isOnStream = false;
function getUrlVars() {
  var vars = {},
    hash;
  var hashes = window.location.href
    .slice(window.location.href.indexOf("?") + 1)
    .split("&");
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split("=");
    if (hash[1] !== undefined) vars[hash[0]] = hash[1];
  }
  return vars;
}

function openStream() {
  const config = { audio: true, video: true };
  return navigator.mediaDevices.getUserMedia(config);
}

function playStream(idVideoTag, stream) {
  const video = document.getElementById(idVideoTag);
  video.srcObject = stream;
  window.peer_stream = stream;
}

$(document).ready(() => {
  let query = getUrlVars();
  window.onbeforeunload = () => {
    socket.emit("Client-webRtc-send-cancel-call", {
      userId,
      answerSocketId,
      answerId: answerWebRtcId,
    });
    peer.destroy();
  };

  userId = query.userId;
  answerWebRtcId = query.answerId;
  answerSocketId = query.answerSocketId;
  isCaller = query.isCaller;
  socketId = query.socketId;
  console.log(userId, answerWebRtcId, answerSocketId, isCaller, socketId);
  socket.emit("Client-send-socket", {
    socketId: socket.id,
    userId,
  });
  peer = new Peer(socketId, {
    host: "webrtc.pdchatapp.tk",
    port: 443,
    path: "webRtc",
    config: {
      iceServers: [
        { url: "stun:stun01.sipphone.com" },
        { url: "stun:stun.ekiga.net" },
        { url: "stun:stun.fwdnet.net" },
        { url: "stun:stun.ideasip.com" },
        { url: "stun:stun.iptel.org" },
        { url: "stun:stun.rixtelecom.se" },
        { url: "stun:stun.schlund.de" },
        { url: "stun:stun.l.google.com:19302" },
        { url: "stun:stun1.l.google.com:19302" },
        { url: "stun:stun2.l.google.com:19302" },
        { url: "stun:stun3.l.google.com:19302" },
        { url: "stun:stun4.l.google.com:19302" },
        { url: "stun:stunserver.org" },
        { url: "stun:stun.softjoys.com" },
        { url: "stun:stun.voiparound.com" },
        { url: "stun:stun.voipbuster.com" },
        { url: "stun:stun.voipstunt.com" },
        { url: "stun:stun.voxgratia.org" },
      ],
    },
  });
  $("#cancel").click(() => {
    socket.emit("Client-webRtc-send-cancel-call", {
      userId,
      answerSocketId,
      answerId: answerWebRtcId,
    });
    peer.destroy();
    window.close();
  });
  peer.on("open", (id) => {
    console.log("connected: ", id);
  });

  peer.on("error", (err) => {
    console.log("webRtc: ", err.type);
  });
  //người nghe
  if (isCaller === "false")
    peer.on("call", (call) => {
      console.log("peer: ", peer);
      openStream()
        .then((stream) => {
          console.log("open stream success");
          call.answer(stream);
          call.on("stream", (remoteStream) => {
            console.log("receiver on stream success: ", remoteStream);
            socket.on("Server-send-webRtc-cancel-call", (data) => {
              console.log("answer socket close");
              peer.destroy();

              call.close();
            });
            call.on("close", () => {
              peer.destroy();

              call.close();
              console.log("answer close");
              socket.emit("Client-webRtc-send-cancel-call", {
                userId,
                answerSocketId,
                answerId: answerWebRtcId,
              });
            });
            if (isOnStream === false) {
              console.log("remote stream: ", remoteStream);
              console.log("local stream: ", stream);
              playStream("video", remoteStream);
              playStream("localVideo", stream);
            }
            isOnStream = true;
          });
        })
        .catch((streamErr) => {
          console.log("open stream error: ", streamErr);
        });
    });

  socket.on("Server-send-webRtc-cancel-call", (data) => {
    console.log("server send cancel call");
    socket.emit("Client-webRtc-send-cancel-call", {
      userId,
      answerSocketId,
      answerId: answerWebRtcId,
    });
    peer.destroy();
    window.close();
  });
  if (isCaller === "true") {
    openStream()
      .then((stream) => {
        const call = peer.call(answerSocketId, stream);
        console.log("caller: open stream success", stream);
        console.log("peer caller: ", call);
        socket.on("Server-send-webRtc-cancel-call", (data) => {
          console.log("caller socket close");
          call.close();
          peer.destroy();
        });
        call.on("close", () => {
          console.log("caller rtc close");
          call.close();
          peer.destroy();

          socket.emit("Client-webRtc-send-cancel-call", {
            userId,
            answerSocketId,
            answerId: answerWebRtcId,
          });
        });
        call.on("stream", (remoteStream) => {
          console.log("caller on stream success:", remoteStream);
          if (isOnStream === false) {
            console.log("remote stream: ", remoteStream);
            console.log("local stream: ", stream);
            playStream("video", remoteStream);
            playStream("localVideo", stream);
          }
          isOnStream = true;
        });
      })
      .catch((streamErr) => {
        console.log("open stream error: ", streamErr);
      });
  }
});
