const serverUrl = "backend.pdchatapp.tk";
const openWindows = "https://backend.pdchatapp.tk";
const socket = io(`${serverUrl}`);
let peer;
let webRtcId;
let token = "";
let sendingData;
let user;
let roomId;
let answerWebRtcId;
let answerSocketId;
let isOnCall = false;
let isCaller = false;

//music
let musicConnectId;
let musicUrl;
let isMusicRequester = false;
let isOnMusic = false;
let musicConnectSocketId;
$(document).on("click", "#chat-list > ul > li > button", function (event) {
  sendingData = {
    userName: this.name,
    sendingId: this.id,
  };
  $("#chat-list").hide();
  $("#name-chat").text(
    sendingData.userName + " - id: " + sendingData.sendingId
  );
  $("#chat").show();
  socket.emit("Client-join-room", {
    sendingId: this.id,
    userId: user._id,
  });
  socket.on("Server-send-roomId", (data) => {
    roomId = data.roomId;
    console.log("join room: ", roomId);
  });
});
$(document).ready(function () {
  socket.on("disconnect", (reason) => {
    socket.connect();
    console.log(sendingData);
    socket.emit("Client-join-room", {
      sendingId: sendingData.sendingId,
      userId: user._id,
    });
    socket.emit("Client-send-socket", {
      userId: user._id,
    });
    // peer.destroy();
    // peer = new Peer(socket.id, {
    //   host: "webrtc.pdchatapp.tk",
    //   port: 443,
    //   path: "/webRtc",
    // });
  });
  //Nhận dữ liệu
  socket.on("Server-send-name", function (data) {
    $("#name").text(data);
  });
  socket.on("Server-send-message", function (data) {
    if (data.userId !== user._id)
      $("#messages").append(
        `<br><p style="position: relative; left: 300px;">${data.userName}: ${data.message}</p>`
      );
    else $("#messages").append(`<br><p">${data.userName}: ${data.message}</p>`);
  });
  socket.on("Server-send-notify", function (data) {
    $("#notify").text(`Nội dung: ${data.notify} - time: ${data.time}`);
  });

  socket.on("Server-send-list-room", (data) => {
    console.log("list room: ", data);
  });
  ///
  //Sau khi bấm gọi đi
  socket.on("Server-webRtc-send-call-response", (data) => {
    console.log(data);
    const { success, code, message } = data;
    if (success) {
      $("#btnCancel").show();
      $("#notify").text("Waiting receiver answer");
    } else $("#notify").text(message);
  });
  // Nguoi nhận ko muốn nhận cuộc gọi
  socket.on("Server-send-webRtc-not-receive-call", (data) => {
    console.log("receiver cancel call: ", data);
    const { receiverId, receiverSocketId } = data;
    $("#btnAnswer").hide();
    $("#btnCancel").hide();
    $("#notify").text("Receiver not answer");
    socket.emit("Client-send-webRtc-cancel-call", {
      answerId: receiverId,
      answerSocketId: receiverSocketId,
    });
  });
  //Một client của account đó ko muốn nhận cuộc gọi
  socket.on("Server-send-webRtc-me-not-receive-call", (data) => {
    const { receiverId, receiverSocketId } = data;
    $("#btnAnswer").hide();
    $("#btnCancel").hide();
    $("#notify").text("some client cancel call");
    socket.emit("Client-send-webRtc-cancel-call", {
      answerId: receiverId,
      answerSocketId: receiverSocketId,
    });
  });
  //Nguoi gọi ko muốn gọi nữa
  socket.on("Server-send-webRtc-not-want-call", (data) => {
    console.log("caller not want call: ", data);
    $("#btnAnswer").hide();
    $("#btnCancel").hide();
    $("#notify").text("Caller cancel call");
  });
  //Có cuộc gọi đến
  socket.on("Server-send-webRtc-have-a-call", function (data) {
    if (data.code === "haveACall") {
      console.log(data.response);
      answerWebRtcId = data.response.callerId;
      answerSocketId = data.response.callerSocketId;
      $("#btnAnswer").show();
      $("#btnCancel").show();
      $("#notify").text(`${data.message}`);
    }
  });
  //Sau khi nhận chấp nhận cuộc gọi
  socket.on("Server-webRtc-send-answer-response", (data) => {
    const { success, message, code, response } = data;
    console.log(data);
    if (data.success) {
      //Người gọi
      console.log("answer");
      const { peerId, receiverId } = response;
      const remoteId = peerId;
      answerSocketId = peerId;
      answerWebRtcId = receiverId;
      window.open(
        `${openWindows}/video?isCaller=true&userId=${user._id}&answerId=${answerWebRtcId}&&answerSocketId=${answerSocketId}&socketId=${socket.id}`,
        "video call",
        "width=1000,height=600"
      );
      // openStream().then((stream) => {
      //   $("#btnAnswer").hide();
      //   $("#btnCancel").show();
      //   const call = peer.call(remoteId, stream);
      //   console.log("peer caller: ", call);
      //   socket.on("Server-send-webRtc-cancel-call", (data) => {
      //     $("#notify").text("call cancel");
      //     $("#btnAnswer").hide();
      //     $("#btnCancel").hide();
      //     call.close();
      //   });
      //   call.on("close", () => {
      //     console.log("close");
      //     isOnCall = false;
      //     isCaller = false;
      //     let s = stream.getTracks();
      //     s.forEach(function (track) {
      //       track.stop();
      //     });
      //     delete stream;
      //     call.close();
      //     $("#btnAnswer").hide();
      //     $("#btnCancel").hide();
      //     socket.emit("Client-webRtc-send-cancel-call", {
      //       userId: user._id,
      //       answerSocketId,
      //       answerId: answerWebRtcId,
      //     });
      //   });
      //   call.on("stream", (remoteStream) => {
      //     playStream("localStream", stream);
      //     playStream("remoteStream", remoteStream);
      //     localRemoteStream = remoteStream;
      //     isOnCall = true;
      //   });
      // });
    } else $("#notify").text(message);
  });

  //Nếu có client khác nhận cuộc gọi
  socket.on("Server-webRtc-send-another-client-receive-call", (data) => {
    $("#btnAnswer").hide();
    $("#btnCancel").hide();
    $("#notify").text("Some client has answered call");
  });
  //Nhận về xem gửi request thành công không
  socket.on("Server-music-send-request-music-response", (data) => {
    if (data.success) {
      $("#btnCancelRequestMusic").show();
      $("#btnRequestMusic").hide();
      $("#notify").text("đợi bên nghe đồng ý");
    } else {
      $("#btnCancelRequestMusic").hide();
      $("#btnRequestMusic").show();
      $("#notify").text(data.message);
    }
  });
  //Nhận về 1 client của user này tắt kết nối
  socket.on("Server-music-send-not-want-accept-to-another-client", (data) => {
    $("#btnAcceptRequestMusic").hide();
    $("#btnCancelRequestMusic").hide();
    $("#btnRequestMusic").show();
    $("#notify").text("Có một người dùng trong số bạn không đồng ý nghe");
  });
  //Nhận về event hỏi có nghe nhạc không
  socket.on("Server-music-send-have-a-music-request", (data) => {
    $("#btnAcceptRequestMusic").show();
    $("#btnCancelRequestMusic").show();
    $("#btnRequestMusic").hide();
    $("#notify").text("Có người mời nghe nhạc chung");

    musicConnectId = data.musicConnectId;
    musicUrl = data.musicUrl;
    musicConnectSocketId = data.musicConnectSocketId;
  });
  //Nhận về đồng ý nghe nhạc chung
  socket.on("Server-music-send-accept-music-request-response", (data) => {
    console.log("accept: ", data);
    $("#btnAcceptRequestMusic").hide();
    $("#music").append(`
        <audio autoplay loop id="audio">
      <source src="http://${musicUrl}" type="audio/mpeg">
      Your browser does not support the audio tag.
    </audio>`);
    isOnMusic = true;
  });
  //Dừng nghe nhạc chung
  socket.on("Server-music-send-stop-music", (data) => {
    $("#audio").remove();
    $("#btnCancelRequestMusic").hide();
    $("#btnRequestMusic").show();
    $("#notify").text("Hai bên dừng nghe nhạc chung");
  });
  //Nhận về không muốn gửi đi việc nghe nhạc nữa
  socket.on("Server-music-send-not-want-send-request", (data) => {
    musicUrl = undefined;
    musicConnectId = undefined;
    isMusicRequester = undefined;
    $("#btnAcceptRequestMusic").hide();
    $("#btnCancelRequestMusic").hide();
    $("#btnRequestMusic").show();
    $("#notify").text("Người gửi không muốn kết nối tiếp");
  });
  //Nhận về là không muốn nhận lời mời nghe nhạc nữa
  socket.on("Server-music-send-not-want-accept", (data) => {
    console.log("Server-music-send-not-want-accept");
    musicUrl = undefined;
    musicConnectId = undefined;
    isMusicRequester = undefined;
    $("#btnAcceptRequestMusic").hide();
    $("#btnCancelRequestMusic").hide();
    $("#btnRequestMusic").show();
    $("#notify").text("Người nhận không đồng ý kết nối");
  });
  socket.on("Server-music-send-not-want-accept", (data) => {
    musicUrl = undefined;
    musicConnectId = undefined;
    isMusicRequester = undefined;
    $("#btnAcceptRequestMusic").hide();
    $("#btnCancelRequestMusic").hide();
    $("#btnRequestMusic").show();
    $("#notify").text("Có một client không muốn kết nối");
  });
  //Có client đã chấp thuận nghe nhạc
  socket.on("Server-music-send-another-accept-music-request", (data) => {
    console.log("another accept");
    musicUrl = undefined;
    musicConnectId = undefined;
    isMusicRequester = undefined;
    $("#btnAcceptRequestMusic").hide();
    $("#btnCancelRequestMusic").hide();
    $("#btnRequestMusic").show();
    $("#notify").text("Đã có client chấp thuận nghe nhạc");
  });
  //Gửi dữ liệu
  $("#btnLogin").click(login);
  $("#btnGetInfo").click(getUserInfo);
  $("#btn-send-message").click(sendMessage);
  $("#leave-room").click(leaveRoom);
  $("#btnCall").click(callWebRtc);
  $("#btnAnswer").click(answerWebRtc);
  $("#btnCancel").click(cancelWebRtc);
  $("#btnRequestMusic").click(requestMusic);
  $("#btnAcceptRequestMusic").click(acceptMusic);
  $("#btnCancelRequestMusic").click(cancelMusic);

  //
});
function cancelMusic() {
  $("#audio").remove();
  $("#btnAcceptRequestMusic").hide();
  $("#btnCancelRequestMusic").hide();
  $("#btnRequestMusic").show();

  if (isOnMusic) {
    socket.emit("Client-music-send-stop-music", {
      musicConnectId,
      musicUrl,
      userId: user._id,
    });
  } else if (isMusicRequester) {
    socket.emit("Client-music-send-not-want-send-request", {
      musicConnectId,
      musicUrl,
      userId: user._id,
    });
  } else {
    socket.emit("Client-music-send-not-want-accept", {
      musicConnectId,
      musicUrl,
      userId: user._id,
      musicConnectSocketId,
    });
  }
  musicConnectId = undefined;
  musicUrl = undefined;
  isMusicRequester = undefined;
  isOnMusic = false;
}
function acceptMusic() {
  isOnMusic = true;
  isMusicRequester = false;
  $("#music").append(`
  <audio id="audio" autoplay loop>
<source src="http://${musicUrl}" type="audio/mpeg">
Your browser does not support the audio tag.
</audio>`);
  socket.emit("Client-music-send-accept-music-request", {
    userId: user._id,
    musicConnectId,
    musicConnectSocketId,
  });
  $("#btnAcceptRequestMusic").hide();
}
function requestMusic() {
  musicUrl = $("#musicUrl").val();
  console.log(musicUrl);

  isMusicRequester = true;
  musicConnectId = sendingData.sendingId;
  socket.emit("Client-music-send-request-music", {
    musicConnectId: sendingData.sendingId,
    musicUrl,
    userId: user._id,
  });
  isMusicRequester = true;
}
function openStream() {
  const config = { audio: true, video: true };
  return navigator.mediaDevices.getUserMedia(config);
}

function playStream(idVideoTag, stream) {
  const video = document.getElementById(idVideoTag);
  if (video.paused) {
    video.srcObject = stream;
    video.play();
  }
}

function callWebRtc() {
  socket.emit("Client-webRtc-send-callId", {
    userId: user._id,
    callId: sendingData.sendingId,
  });
  answerWebRtcId = sendingData.sendingId;
  isCaller = true;
}

function answerWebRtc() {
  window.open(
    `${openWindows}/video?isCaller=false&userId=${user._id}&answerId=${answerWebRtcId}&&answerSocketId=${answerSocketId}&socketId=${socket.id}`,
    "video call",
    "width=1000, height=600"
  );
  socket.emit("Client-webRtc-send-answer", {
    userId: user._id,
    answerId: answerWebRtcId,
    answerSocketId,
  });
}

function cancelWebRtc() {
  $("#notify").text("Cancel call");
  if (isOnCall) {
    //một trong 2 người muốn kết thúc
    socket.emit("Client-webRtc-send-cancel-call", {
      userId: user._id,
      answerSocketId,
      answerId: answerWebRtcId,
    });
  } else if (isCaller === false)
    //Người nghe ko muốn nhận
    socket.emit("Client-webRtc-send-not-receive-call", {
      userId: user._id,
      answerSocketId,
      answerId: answerWebRtcId,
    });
  else {
    //Người gọi kết thúc ko muốn gọi
    socket.emit("Client-webRtc-send-not-want-call", {
      userId: user._id,
      answerId: answerWebRtcId,
    });
  }
  $("#btnCancel").hide();
  $("#btnAnswer").hide();

  isCaller = false;
  isOnCall = false;
}

//leave room
function leaveRoom() {
  socket.emit("Client-leave-room", { roomId });
  $("#messages").empty();
  $("#chat").hide();
  $("#chat-list").show();
}
//login
function login() {
  let email = $("#email").val();
  let password = $("#password").val();
  $.ajax({
    url: "/api/users/login",
    type: "POST",
    data: {
      userEmail: email,
      userPassword: password,
    },
    success: function (data) {
      token = data.data.refreshToken;
      $("#login").hide();
      $("#getInfo").show();
      alert("Đăng nhập thành công");
    },
    error: function (data) {
      alert("Sai tên đăng nhập hoặc mật khẩu");
    },
  });
}
//get user info
function getUserInfo() {
  $.ajax({
    url: "/api/users/",
    type: "GET",
    headers: {
      authorization: token,
    },
    success: function (data) {
      user = data.data;
      $("#name").text(user.userName + " - id: " + user._id);
      socket.emit("Client-send-socket", {
        socketId: socket.id,
        userId: user._id,
      });
      // peer = new Peer(socket.id, {
      //   host: "webrtc.pdchatapp.tk",
      //   secure: true,
      //   port: 443,
      //   path: "webRtc",
      //   debug: 3,
      // });
      // peer.on("open", (id) => {
      //   console.log("connected: ", id);
      // });

      // // // //người nghe
      // peer.on("call", (call) => {
      //   console.log("peer: ", peer);
      //   openStream().then((stream) => {
      //     call.answer(stream);
      //     call.on("stream", (remoteStream) => {
      //       isOnCall = true;
      //       $("#btnAnswer").hide();
      //       $("#btnCancel").show();
      //       socket.on("Server-send-webRtc-cancel-call", (data) => {
      //         $("#btnAnswer").hide();
      //         $("#btnCancel").hide();
      //         $("#notify").text("call cancel");
      //         call.close();
      //       });
      //       call.on("close", () => {
      //         isOnCall = false;
      //         isCaller = false;
      //         call.close();
      //         $("#btnAnswer").hide();
      //         $("#btnCancel").hide();
      //         let s = stream.getTracks();
      //         s.forEach(function (track) {
      //           track.stop();
      //         });
      //         s = remoteStream.getTracks();
      //         s.forEach(function (track) {
      //           track.stop();
      //         });
      //         socket.emit("Client-webRtc-send-cancel-call", {
      //           userId: user._id,
      //           answerSocketId,
      //           answer: answerWebRtcId,
      //         });
      //         delete stream;
      //         delete remoteStream;
      //       });
      //       playStream("localStream", stream);
      //       playStream("remoteStream", remoteStream);
      //     });
      //   });
      // });

      // peer.on("error", (err) => {
      //   console.log("webRtc: ", err.type);
      // });
    },
    error: function (err) {
      console.log(err);
    },
  });

  $.ajax({
    url: "/api/friends",
    type: "GET",
    headers: {
      authorization: token,
    },
    success: function (data) {
      $("#getInfo").hide();
      $("#chat-list").show();
      let friends = data.data;
      for (let i = 0; i < friends.length; i++) {
        $("#chat-list > ul")
          .append(`<li id=${friends[i].userFriendId}>${friends[i].userName} 
        <button id="${friends[i].userFriendId}" name="${friends[i].userName} ">Chat</button></li>
        `);
      }
      console.log(data);
    },
    error: function (err) {
      $("#getInfo").hide();
      $("#chat-list").show();
    },
  });
}
//send message
function sendMessage() {
  let message = $("#send-message").val();
  let data = {
    roomId,
    message,
    userName: user.userName,
    userId: user._id,
    sendingId: sendingData.sendingId,
    type: "text",
  };
  $("#messages").append(`<br><p>${user.userName}: ${message}</p>`);
  socket.emit("Client-send-message", data);
  // socket.emit("Client-send-notify", {notifyTo: sendingData.sendingId,
  // title: `Có tin nhắn mới từ ${user.userName}`,
  // time:Date.now(),
  // content: message})
}
