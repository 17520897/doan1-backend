const serverSendWebRtcHaveACall = async (
  socket,
  clientSocketIds,
  sendingId,
  notifyData
) => {
  if (clientSocketIds[sendingId]) {
    for (let i = 0; i < clientSocketIds[sendingId].length; i++) {
      socket
        .to(clientSocketIds[sendingId][i])
        .emit("Server-send-webRtc-have-a-call", notifyData);
    }
  }
};

module.exports = serverSendWebRtcHaveACall;
