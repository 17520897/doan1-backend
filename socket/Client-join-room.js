const DAO = require("../classes/DAO");
const randomString = require("randomstring");
const clientJoinRoom = (socket) => {
  socket.on("Client-join-room", async (data) => {
    console.log("client send data to join room: ", data);
    if (data.userId && data.sendingId) {
      let userRoomsDAO = new DAO("userRooms");
      let roomId;
      let room = await userRoomsDAO.getOne({
        $and: [{ userId: data.userId, "rooms.userConnectId": data.sendingId }],
      });
      if (room.success) {
        room = await userRoomsDAO.getOne(
          {
            userId: data.userId,
            "rooms.userConnectId": data.sendingId,
          },
          { "rooms.$": 1 }
        );
        if (!room.success)
          //Lỗi kết nối
          socket.emit("Server-send-error", {
            message: "Lỗi kết nối",
          });
        roomId = room.payload.rooms[0].roomId;
      } else {
        const randomRoomId = `${randomString.generate({
          length: 8,
        })}${Date.now()}`;
        roomId = randomRoomId;
        //Tạo rooms cho userId
        room = await userRoomsDAO.getOne({ userId: data.userId });
        if (room.success) {
          await userRoomsDAO.updateOne(
            { userId: data.userId },
            {
              $push: {
                rooms: { roomId: randomRoomId, userConnectId: data.sendingId },
              },
            }
          );
        } else {
          await userRoomsDAO.create({
            userId: data.userId,
            rooms: [
              {
                roomId: randomRoomId,
                userConnectId: data.sendingId,
              },
            ],
          });
        }
        //Tạo room cho sendingId
        room = await userRoomsDAO.getOne({ userId: data.sendingId });
        if (room.success) {
          await userRoomsDAO.updateOne(
            { userId: data.sendingId },
            {
              $push: {
                rooms: { roomId: randomRoomId, userConnectId: data.userId },
              },
            }
          );
        } else {
          await userRoomsDAO.create({
            userId: data.sendingId,
            rooms: [
              {
                roomId: randomRoomId,
                userConnectId: data.userId,
              },
            ],
          });
        }
      }
      socket.join(roomId);
      console.log(`User: ${data.userId} join room: ${roomId}`);
      socket.emit("Server-send-roomId", { roomId });
      console.log("Send room Id to client");
    } else {
      socket.emit("Server-send-error", {
        message: "Thiếu data",
        data,
      });
    }
  });
};

module.exports = clientJoinRoom;
