const DAO = require("../classes/DAO");
const notifyConst = require("./notifyConst");
const serverSendNotify = require("./serverSendNotify");
const clientSendFriendRequest = async (socket, clientSocketIds) => {
  socket.on("Client-send-friend-request", async (data) => {
    const userDAO = new DAO("users");
    const { userId, sendingId } = data;
    let sendingUser = await userDAO.getById(userId);
    serverSendNotify(socket, clientSocketIds, sendingId, {
      notify: notifyConst.newFriend + sendingUser.payload.userName,
      userId: sendingId,
      sendingId: userId,
      type: "newFriend",
      time: Date.now(),
    });
  });
};

module.exports = clientSendFriendRequest;
