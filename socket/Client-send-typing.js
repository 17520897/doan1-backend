const DAO = require("../classes/DAO");
const serverSendNotify = require("./serverSendNotify");
const clientSendFriendRequest = async (socket) => {
  socket.on("Client-send-typing", async (data) => {
    const { roomId, userId } = data;
    const userDAO = new DAO("users");
    const user = await userDAO.getById(userId);
    if (user.success) {
      socket.broadcast.to(roomId).emit("Server-send-typing", {
        roomId,
        userId,
        isTyping: true,
      });
    }
  });
};

module.exports = clientSendFriendRequest;
