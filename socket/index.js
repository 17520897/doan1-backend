const DAO = require("../classes/DAO");
const serverSendNotify = require("./serverSendNotify");
const serverSendWebRtcHaveACall = require("./serverSendWebRtcHaveACall");
const socketServer = async (server) => {
  const io = require("socket.io").listen(server);
  let clientSocketIds = {};
  let socketIds = {};
  let onCalls = {};
  let onWaiting = {};
  let onMusic = {};
  const socketDAO = new DAO("socket");
  io.on("connection", async (socket) => {
    //#region messages
    console.log("new user: ", socket.id);
    //truyền dữ liệu
    socket.emit("Server-send-name", socket.id);
    //nhận dữ liệu
    //Join người dùng vào 1 room để nt
    require("./Client-join-room")(socket);

    //lưu danh sách socket theo users
    socket.on("Client-send-socket", async (data) => {
      socketIds[socket.id] = data.userId;
      if (clientSocketIds[data.userId]) {
        clientSocketIds[data.userId].push(socket.id);
      } else {
        clientSocketIds[data.userId] = [socket.id];
      }
      if (!onCalls[data.userId]) onCalls[data.userId] = false;
      if (!onWaiting[data.userId]) onWaiting[data.userId] = false;
      if (!onMusic[data.userId]) onMusic[data.userId] = false;
    });

    //Gửi lời mời kết bạn
    require("./Client-send-friend-request")(socket, clientSocketIds);
    //Gửi chấp thuận kết bạn
    require("./Client-send-accept-friend")(socket, clientSocketIds);

    //Gửi tin nhắn vào một room
    require("./Client-send-message")(socket, clientSocketIds, socketIds);

    require("./Client-send-typing")(socket);

    //Rời khỏi room
    socket.on("Client-leave-room", (data) => {
      console.log(`Socket: ${socket.id} join room: ${data.roomId}`);
      socket.leave(data.roomId);
    });
    //Ngắt kết nối xóa socketId khỏi danh sách
    socket.on("disconnect", (data) => {
      const clientId = socketIds[socket.id];
      console.log(socket.id, " disconnect!");
      if (socketIds[socket.id])
        console.log(socketIds[socket.id], "disconnect!");
      delete socketIds[socket.id];
      onCalls[clientId] = false;
      onWaiting[clientId] = false;
      onMusic[clientId] = false;
      //remove socketId from clientSocketId
      if (clientSocketIds[clientId])
        for (let i = 0; i < clientSocketIds[clientId].length; i++) {
          if (clientSocketIds[clientId][i].toString() == socket.id.toString())
            clientSocketIds[clientId].splice(i, 1);
        }
    });
    //#endregion messages
    //#region WEBRTC
    //listen
    //Client trả lời cuộc gọi
    socket.on("Client-webRtc-send-answer", (data) => {
      const { userId, answerId, answerSocketId } = data;
      if (onCalls[userId]) {
        socket.emit("Server-webRtc-send-answer-response", {
          message: "you is on call",
          code: "youOnCall",
          success: false,
        });
      } else if (onCalls[answerId]) {
        socket.emit("Server-webRtc-send-answer-response", {
          message: "caller On Call",
          code: "callerOnCall",
          success: false,
        });
      } else {
        onCalls[userId] = true;
        onCalls[answerId] = true;
        console.log(data);
        socket.to(answerSocketId).emit("Server-webRtc-send-answer-response", {
          message: "answer success",
          code: "answer",
          success: true,
          response: {
            peerId: socket.id,
            receiverId: userId,
          },
        });
        //Gửi lại cho tk answer ngoại trừ người bấm gọi. Đã có client của nó nhận
        if (clientSocketIds[userId])
          for (let i = 0; i < clientSocketIds[userId].length; i++) {
            if (userId !== clientSocketIds[userId][i]) {
              console.log("send client receive call");
              socket
                .to(clientSocketIds[userId][i])
                .emit("Server-webRtc-send-another-client-receive-call", {
                  response: {
                    peerId: socket.id,
                    message: "another client answer call",
                  },
                });
            }
          }
      }
    });
    //Client muốn gọi đến 1 người
    socket.on("Client-webRtc-send-callId", (data) => {
      const { callId, userId } = data;
      console.log("call: ", data);
      if (clientSocketIds[callId]) {
        console.log(onCalls[userId], onWaiting[userId]);
        if (onCalls[userId] || onWaiting[userId]) {
          console.log(onCalls[userId], onWaiting[userId]);
          console.log("youOnCall");
          socket.emit("Server-webRtc-send-call-response", {
            message: "You is on another call",
            code: "youOnCall",
            success: false,
          });
        } else if (onCalls[callId] || onWaiting[callId]) {
          console.log("receiverOnCall");
          socket.emit("Server-webRtc-send-call-response", {
            message: "Receiver is on another call",
            code: "receiverOnCall",
            success: false,
          });
        } else {
          console.log("calling");
          serverSendWebRtcHaveACall(socket, clientSocketIds, data.callId, {
            message: "You have a call",
            code: "haveACall",
            success: true,
            response: {
              callerId: userId,
              callerSocketId: socket.id,
            },
          });
          socket.emit("Server-webRtc-send-call-response", {
            message: "Call success",
            code: "callSuccess",
            success: true,
            response: data,
          });
          // Khi có cuộc gọi thì người khác ko gọi tới tiếp được
          onWaiting[userId] = true;
          onWaiting[callId] = true;
        }
      } else {
        socket.emit("Server-webRtc-send-call-response", {
          message: "Receiver is offline",
          code: "receiverOffline",
          success: false,
        });
      }
    });

    //Client muốn hủy cuộc gọi
    socket.on("Client-webRtc-send-cancel-call", (data) => {
      const { answerSocketId, answerId, userId } = data;
      console.log("cancel: ", data);
      if (onCalls[userId]) onCalls[userId] = false;
      if (onWaiting[userId]) onWaiting[userId] = false;
      if (onCalls[answerId]) onCalls[answerId] = false;
      if (onWaiting[answerId]) onWaiting[answerId] = false;
      console.log("userId: ", onCalls[userId], onWaiting[userId]);
      console.log("answerId: ", onCalls[answerId], onWaiting[answerId]);
      socket.emit("Server-send-webRtc-cancel-call", {});
      //socket.to(answerSocketId).emit("Server-send-webRtc-cancel-call", {});
      if (clientSocketIds[answerId]) {
        for (let i = 0; i < clientSocketIds[answerId].length; i++) {
          socket
            .to(clientSocketIds[answerId][i])
            .emit("Server-send-webRtc-cancel-call", {});
        }
      }
    });
    //Người nhận hủy cuộc gọi
    socket.on("Client-webRtc-send-not-receive-call", (data) => {
      const { userId, answerSocketId, answerId } = data;
      if (onCalls[userId]) onCalls[userId] = false;
      if (onWaiting[userId]) onWaiting[userId] = false;
      if (onCalls[answerId]) onCalls[answerId] = false;
      if (onWaiting[answerId]) onWaiting[answerId] = false;
      socket.emit("Server-send-webRtc-cancel-call", {});
      // socket.emit("Server-send-webRtc-not-receive-call", {
      //   receiverSocketId: socket.id,
      //   receiverId: userId,
      // });
      socket.to(answerSocketId).emit("Server-send-webRtc-not-receive-call", {
        receiverSocketId: socket.id,
        receiverId: userId,
      });
      if (clientSocketIds[userId]) {
        for (let i = 0; i < clientSocketIds[userId].length; i++) {
          socket
            .to(clientSocketIds[userId][i])
            .emit("Server-send-webRtc-me-not-receive-call", {
              receiverSocketId: socket.id,
              receiverId: userId,
            });
        }
      }
    });
    //Người gọi hủy cuộc gọi
    socket.on("Client-webRtc-send-not-want-call", (data) => {
      console.log("caller cancel call: ", data);
      const { userId, answerSocketId, answerId } = data;
      if (onCalls[userId]) onCalls[userId] = false;
      if (onWaiting[userId]) onWaiting[userId] = false;
      if (onCalls[answerId]) onCalls[answerId] = false;
      if (onWaiting[answerId]) onWaiting[answerId] = false;
      socket.emit("Server-send-webRtc-cancel-call", {});
      if (clientSocketIds[answerId]) {
        for (let i = 0; i < clientSocketIds[answerId].length; i++) {
          socket
            .to(clientSocketIds[answerId][i])
            .emit("Server-send-webRtc-not-want-call", {});
        }
      }
    });

    //emit
    //#endregion WEBRTC
    //#region music
    socket.on("Client-music-send-request-music", (data) => {
      console.log("user request music: ", data);
      const { userId, musicConnectId, musicUrl, musicName } = data;
      if (!clientSocketIds[musicConnectId])
        return socket.emit("Server-music-send-request-music-response", {
          success: false,
          message: "người dùng không online",
        });
      if (onMusic[musicConnectId])
        return socket.emit("Server-music-send-request-music-response", {
          success: false,
          message: "Người kia đang nghe nhạc với người khác",
        });
      if (onMusic[userId])
        return socket.emit("Server-music-send-request-music-response", {
          success: false,
          message: "Bạn nghe nhạc với một người khác",
        });
      onMusic[userId] = true;
      socket.emit("Server-music-send-request-music-response", {
        success: true,
        message: "send music request success",
      });
      if (clientSocketIds[musicConnectId]) {
        for (let i = 0; i < clientSocketIds[musicConnectId].length; i++) {
          socket
            .to(clientSocketIds[musicConnectId][i])
            .emit("Server-music-send-have-a-music-request", {
              message: "have a music request",
              musicConnectId: userId,
              musicConnectSocketId: socket.id,
              musicUrl,
              musicName,
            });
        }
      }
    });
    //Chấp thuận nghe nhạc chung
    socket.on("Client-music-send-accept-music-request", (data) => {
      const { musicConnectId, userId, musicConnectSocketId } = data;
      if (onMusic[musicConnectId] === false) onMusic[musicConnectId] = true;
      if (onMusic[userId] === false) onMusic[userId] = true;
      console.log("client accpet music: ", data);

      socket
        .to(musicConnectSocketId)
        .emit("Server-music-send-accept-music-request-response", {
          success: true,
          message: "accept music",
        });

      if (clientSocketIds[userId]) {
        for (let i = 0; i < clientSocketIds[userId].length; i++) {
          if (clientSocketIds[userId][i] !== socket.id)
            socket
              .to(clientSocketIds[userId][i])
              .emit("Server-music-send-another-accept-music-request", {});
        }
      }
    });
    socket.on("Client-music-send-stop-music", (data) => {
      const { musicConnectId, userId } = data;
      if (onMusic[musicConnectId]) onMusic[musicConnectId] = false;
      if (onMusic[userId]) onMusic[userId] = false;
      if (clientSocketIds[musicConnectId]) {
        for (let i = 0; i < clientSocketIds[musicConnectId].length; i++) {
          socket
            .to(clientSocketIds[musicConnectId][i])
            .emit("Server-music-send-stop-music", {});
        }
      }
      socket.emit("Server-music-send-stop-music");
    });
    socket.on("Client-music-send-not-want-send-request", (data) => {
      const { musicConnectId, userId } = data;
      onMusic[userId] = false;
      if (clientSocketIds[musicConnectId]) {
        for (let i = 0; i < clientSocketIds[musicConnectId].length; i++) {
          socket
            .to(clientSocketIds[musicConnectId][i])
            .emit("Server-music-send-not-want-send-request", {});
        }
      }
    });
    socket.on("Client-music-send-not-want-accept", (data) => {
      const { musicConnectId, userId, musicConnectSocketId } = data;
      onMusic[musicConnectId] = false;
      socket
        .to(musicConnectSocketId)
        .emit("Server-music-send-not-want-accept", {});
      if (clientSocketIds[userId]) {
        for (let i = 0; i < clientSocketIds[userId].length; i++) {
          if (clientSocketIds[userId][i] !== socket.id)
            socket
              .to(clientSocketIds[userId][i])
              .emit("Server-music-send-not-want-accept-to-another-client", {});
        }
      }
    });
    //#endregion music
  });
};
module.exports = socketServer;
