const serverSendNotify = async (
  socket,
  clientSocketIds,
  sendingId,
  notifyData
) => {
  if (clientSocketIds[sendingId]) {
    for (let i = 0; i < clientSocketIds[sendingId].length; i++) {
      socket
        .to(clientSocketIds[sendingId][i])
        .emit("Server-send-notify", notifyData);
    }
  }
};

module.exports = serverSendNotify;
