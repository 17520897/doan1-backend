const DAO = require("../classes/DAO");
const notifyConst = require("./notifyConst");
const serverSendNotify = require("./serverSendNotify");
const clientSendAcceptFriend = async (socket, clientSocketIds) => {
  socket.on("Client-send-accept-friend", async (data) => {
    const userDAO = new DAO("users");
    const { userId, sendingId } = data;
    let sendingUser = await userDAO.getById(userId);
    serverSendNotify(socket, clientSocketIds, sendingId, {
      notify: sendingUser.payload.userName + notifyConst.acceptFriend,
      userId: sendingId,
      sendingId: userId,
      type: "acceptFriend",
      time: Date.now(),
    });
  });
};

module.exports = clientSendAcceptFriend;
