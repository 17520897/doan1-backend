const DAO = require("../classes/DAO");
const notifyConst = require("./notifyConst");
const serverSendNotify = require("./serverSendNotify");
const clientSendMessage = (socket, clientSocketIds, socketIds) => {
  socket.on("Client-send-message", async (data) => {
    const roomDAO = new DAO("rooms");
    const userDAO = new DAO("users");
    console.log(`send message: `, data);
    const { roomId, message, userId, sendingId, type } = data;
    let room = await roomDAO.getOne({ roomId });
    if (room.success) {
      await roomDAO.updateOne(
        { roomId },
        { $push: { messages: { message, userId, type } } }
      );
    } else {
      await roomDAO.create({
        roomId,
        messages: [
          {
            userId,
            message,
            type,
          },
        ],
      });
    }
    let sendingUser = await userDAO.getById(userId);
    //Gửi tin nhắn đến room
    socket.broadcast.to(data.roomId).emit("Server-send-message", {
      ...data,
    });
    socket.to(data.roomId).emit("Server-send-update-list-room", {
      roomId,
      lastMessage: {
        message: data.message,
        connectName: sendingUser.payload.userName,
        connectAvatar: sendingUser.payload.userAvatar,
        connectId: sendingUser.payload._id,
        time: Date.now(),
      },
    });
    //Gửi thông báo đến người nhận
    serverSendNotify(socket, clientSocketIds, sendingId, {
      notify: notifyConst.message + sendingUser.payload.userName,
      userId: sendingId,
      sendingId: userId,
      type: "message",
      time: Date.now(),
    });
    // if (clientSocketIds[sendingId]) {
    //   for (let i = 0; i < clientSocketIds[sendingId].length; i++) {
    //     socket
    //       .to(clientSocketIds[sendingId][i])
    //       .emit("Server-send-update-list-room", {
    //         roomId,
    //         lastMessage: {
    //           message: data.message,
    //           connectName: sendingUser.payload.userName,
    //           connectAvatar: sendingUser.payload.userAvatar,
    //           connectId: sendingUser.payload._id,
    //         },
    //       });
    //   }
    // }
  });
};

module.exports = clientSendMessage;
