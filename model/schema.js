var mongoose = require("mongoose");
var schema = require("./schema/index");

module.exports = {
  users: mongoose.model("users", schema.users),
  verify: mongoose.model("verify", schema.verify),
  config: mongoose.model("config", schema.config),
  friendRequest: mongoose.model("friendRequest", schema.friendRequest),
  friendPending: mongoose.model("friendPending", schema.friendPending),
  friends: mongoose.model("friends", schema.friends),
  rooms: mongoose.model("rooms", schema.rooms),
  userRooms: mongoose.model("userRooms", schema.userRooms),
  notify: mongoose.model("notify", schema.notify),
  musics: mongoose.model("musics", schema.musics),
};
