const { Schema } = require("mongoose");

const userRooms = {
  userId: {
    type: String,
  },
  rooms: [
    {
      roomId: {
        type: String,
      },
      userConnectId: {
        type: Schema.Types.ObjectId,
      },
    },
  ],
};

module.exports = userRooms;
