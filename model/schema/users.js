const { Schema } = require("mongoose");

const users = {
  userName: {
    type: String,
    required: true,
  },
  userEmail: {
    type: String,
    require: true,
    unique: true,
  },
  userPassword: {
    type: String,
    required: true,
  },
  userGender: {
    type: String,
    required: true,
  },
  userAvatar: {
    type: String,
    default: `${process.env.SERVER_URL}/defaultAvatar.png`,
  },
  isActive: {
    type: Boolean,
    required: true,
    default: false,
  },
};

module.exports = users;
