const { Schema } = require("mongoose");

const friendPending = {
  userId: {
    type: Schema.Types.ObjectId,
  },
  friendPending: [
    {
      userPendingId: {
        type: Schema.Types.ObjectId,
      },
      requestTime: {
        type: Date,
        default: Date.now,
      },
    },
  ],
};

module.exports = friendPending;
