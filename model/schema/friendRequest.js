const { Schema } = require("mongoose");

const friendRequest = {
  userId: {
    type: Schema.Types.ObjectId,
  },
  friendRequests: [
    {
      userRequestId: {
        type: Schema.Types.ObjectId,
      },
      requestTime: {
        type: Date,
        default: Date.now,
      },
    },
  ],
};

module.exports = friendRequest;
