const { Schema } = require("mongoose");

const musics = {
  name: {
    type: String,
    required: true,
  },
  singer: {
    type: String,
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
};

module.exports = musics;
