const { Schema } = require("mongoose");

const friend = {
  userId: {
    type: Schema.Types.ObjectId,
  },
  friends: [
    {
      friendId: {
        type: Schema.Types.ObjectId,
      },
      acceptTime: {
        type: Date,
        default: Date.now,
      },
    },
  ],
};

module.exports = friend;
