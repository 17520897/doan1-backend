const { Schema } = require("mongoose");

const verify = {
  verifyUserId: {
    type: String,
    required: true,
  },
  verifyCode: {
    type: String,
    require: true,
    unique: true,
  },
  verifyExpireAt: {
    type: Date,
    default: Date.now,
    expires: 1800,
  },
  verifyType: {
    type: String,
    required: true,
  },
};

module.exports = verify;
