const { Schema } = require("mongoose");

const rooms = {
  roomId: {
    type: String,
  },
  messages: [
    {
      userId: {
        type: Schema.Types.ObjectId,
      },
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now,
      },
      type: {
        type: String,
        enum: ["text", "image", "sticker", "file"],
      },
      seen: {
        type: Boolean,
        default: false,
      },
    },
  ],
};

module.exports = rooms;
