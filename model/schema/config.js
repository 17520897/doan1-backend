const { Schema } = require("mongoose");

const config = {
  configNumberReportErrorLimit: {
    type: Number,
    default: 10,
  },
  configNumberStickersCollectionLimit: {
    type: Number,
    default: 10,
  },
  configNumberStickerInCollectionLimit: {
    type: Number,
    default: 20,
  },
  configRegisterEmailTemplate: {
    type: String,
  },
  configVerifyEmailTemplate: {
    type: String,
  },
  configNotifyEmailTemplate: {
    type: String,
  },
};

module.exports = config;
