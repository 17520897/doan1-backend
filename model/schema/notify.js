const { Schema } = require("mongoose");

const notify = {
  userId: {
    type: Schema.Types.ObjectId,
  },
  notify: {
    type: String,
  },
  type: {
    type: String,
    enum: ["newFriend", "acceptFriend", "message"],
  },
  time: {
    type: Date,
    default: Date.now,
  },
  sendingId: {
    type: Schema.Types.ObjectId,
  },
  seen: {
    type: Boolean,
    default: false,
  },
};

module.exports = notify;
