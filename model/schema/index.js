module.exports = {
  users: require("./users"),
  verify: require("./verify"),
  config: require("./config"),
  friendRequest: require("./friendRequest"),
  friendPending: require("./friendPending"),
  friends: require("./friends"),
  rooms: require("./rooms"),
  userRooms: require("./userRooms"),
  notify: require("./notify"),
  musics: require("./musics"),
};
