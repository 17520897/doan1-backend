const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getFriendPendingController = async (req, res) => {
  try {
    let friendPendingDAO = new DAO("friendPending");
    let usersDAO = new DAO("users");
    let pending = await friendPendingDAO.getOne({ userId: req.userId });
    if (!pending.success)
      return returnToUser.errorWithMess(res, "Chưa có lời mời kết bạn nào");
    let pendingLength = pending.payload.friendPending.length;
    for (let i = 0; i < pendingLength; i++) {
      let users = await usersDAO.getById(
        pending.payload.friendPending[i].userPendingId
      );
      users = users.payload.toObject();
      pending.payload.friendPending[i] = {
        userName: users.userName,
        userPendingId: users._id,
        userAvatar: users.userAvatar,
        userGender: users.userGender,
        requestTime: pending.payload.friendPending[i].requestTime,
      };
    }
    returnToUser.success(res, "request list", pending.payload.friendPending);
  } catch (err) {
    console.log("getFriendPendingController: ", err);
    returnToUser.errorProcess(res, err);
  }
};

module.exports = getFriendPendingController;
