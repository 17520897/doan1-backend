const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getAllMusicsController = async (req, res) => {
  try {
    const musicDAO = new DAO("musics");
    const musics = await musicDAO.getAll();
    return returnToUser.success(res, "danh sách nhạc", musics);
  } catch (error) {
    console.log("getAllMusicController: ", error);
    return returnToUser.errorProcess(res, error);
  }
};

module.exports = getAllMusicsController;
