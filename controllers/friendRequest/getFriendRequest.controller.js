const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getFriendRequestController = async (req, res) => {
  try {
    let friendRequestDAO = new DAO("friendRequest");
    let usersDAO = new DAO("users");

    let request = await friendRequestDAO.getOne({ userId: req.userId });
    if (!request.success)
      return returnToUser.errorWithMess(res, "Chưa gửi lời mời kết bạn nào");
    let requestLength = request.payload.friendRequests.length;
    for (let i = 0; i < requestLength; i++) {
      let users = await usersDAO.getById(
        request.payload.friendRequests[i].userRequestId
      );
      users = users.payload.toObject();
      request.payload.friendRequests[i] = {
        userName: users.userName,
        userRequestId: users._id,
        userAvatar: users.userAvatar,
        userGender: users.userGender,
        requestTime: request.payload.friendRequests[i].requestTime,
      };
    }
    returnToUser.success(res, "request list", request.payload.friendRequests);
  } catch (err) {
    console.log("getFriendRequestController: ", err);
    returnToUser.errorProcess(res, err);
  }
};

module.exports = getFriendRequestController;
