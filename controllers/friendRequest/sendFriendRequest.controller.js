const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const notifyConst = require("../../socket/notifyConst");
const { userId } = require("../../model/schema/notify");
const sendFriendRequestController = async (req, res) => {
  try {
    let friendRequestDAO = new DAO("friendRequest");
    let friendPendingDAO = new DAO("friendPending");
    const notifyDAO = new DAO("notify");
    const userDAO = new DAO("users");
    let { userRequestId } = req.body;
    let request = await friendRequestDAO.getOne({ userId: req.userId });
    if (request.success) {
      request = await friendRequestDAO.updateOne(
        { userId: req.userId },
        {
          $push: {
            friendRequests: {
              userRequestId,
            },
          },
        },
        {
          new: true,
        }
      );
    } else {
      request = await friendRequestDAO.create({
        userId: req.userId,
        friendRequests: [
          {
            userRequestId,
          },
        ],
      });
    }
    let pending = await friendPendingDAO.getOne({ userId: userRequestId });
    if (pending.success) {
      pending = await friendPendingDAO.updateOne(
        { userId: userRequestId },
        {
          $push: {
            friendPending: {
              userPendingId: req.userId,
            },
          },
        },
        {
          new: true,
        }
      );
    } else {
      pending = await friendPendingDAO.create({
        userId: userRequestId,
        friendPending: [
          {
            userPendingId: req.userId,
          },
        ],
      });
    }
    let user = await userDAO.getById(req.userId);
    if (user.success)
      await notifyDAO.create({
        userId: userRequestId,
        sendingId: req.userId,
        notify: notifyConst.newFriend + user.payload.userName,
        type: "newFriend",
      });
    returnToUser.success(res, "send request success", request.payload);
  } catch (err) {
    console.log(err);
    returnToUser.errorProcess(res, err);
  }
};

module.exports = sendFriendRequestController;
