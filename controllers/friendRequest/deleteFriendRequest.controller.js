const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const deleteFriendRequestController = async (req, res) => {
  try {
    const friendPendingDAO = new DAO("friendPending");
    const friendRequestDAO = new DAO("friendRequest");
    const { userRequestId } = req.body;
    const userId = req.userId;
    let friendPending = await friendPendingDAO.getOne({
      userId: userRequestId,
    });
    if (!friendPending.success)
      return returnToUser.errorWithMess(
        res,
        "Không tìm thấy dữ liệu người dùng"
      );
    let friendRequest = await friendRequestDAO.getOne({
      userId,
    });
    if (!friendRequest.success)
      return returnToUser.errorWithMess(
        res,
        "Không tìm thấy dữ liệu người nhận lời mời"
      );
    await friendPendingDAO.updateById(friendPending.payload._id, {
      $pull: {
        friendPending: {
          userPendingId: req.userId,
        },
      },
    });
    await friendRequestDAO.updateById(friendRequest.payload._id, {
      $pull: {
        friendRequests: {
          userRequestId: userRequestId,
        },
      },
    });
    return returnToUser.successWithNoData(
      res,
      "Xóa lời mời kết bạn thành công"
    );
  } catch (error) {
    console.log("deny friend pending controller: ", error);
    return returnToUser.errorProcess(res, error);
  }
};

module.exports = deleteFriendRequestController;
