const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const mail = require("../../services/mail");
const bcrypt = require("bcryptjs");

const registerUsersController = async (req, res) => {
  try {
    //define
    let userDAO = new DAO("users");
    let verifyDAO = new DAO("verify");
    let configDAO = new DAO("config");
    //create users
    req.body.userPassword = await bcrypt.hash(req.body.userPassword, 10);
    let newUser = await userDAO.create(req.body);
    if (!newUser.success) {
      return returnToUser.errorWithMess(res, newUser.err.message);
    }

    //create verify
    let verifyCode = Date.now().toString(16);
    verifyCode = verifyCode.toUpperCase();
    let newVerify = await verifyDAO.create({
      verifyUserId: newUser.payload._id,
      verifyCode,
      verifyType: "Register",
    });

    //rollback users
    if (!newVerify) {
      await userDAO.deleteById(newUser.payload._id);
      console.log("register user controller - verify code: ", newVerify.err);
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi trong quá trình đăng kí"
      );
    }

    let config = await configDAO.getOne({});
    //rollback users and verify;
    if (!config.success) {
      await userDAO.deleteById(newUser.payload._id);
      await verifyDAO.deleteById(newVerify.payload._id);
      console.log("register user controller - get config: ", newVerify.err);
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi trong quá trình đăng kí"
      );
    }
    //send email
    let subject = "[Đồ án 1 - Chat app] Xác thực tài khoản";
    let template = config.payload.configRegisterEmailTemplate.replace(
      "{code}",
      verifyCode.toString()
    );
    let { userEmail } = newUser.payload;
    let sendEmail = await mail.sendMail(userEmail, subject, template);
    //rollback users and verify
    if (!sendEmail.success) {
      await userDAO.deleteById(newUser.payload._id);
      await verifyDAO.deleteById(newVerify.payload._id);
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi trong quá trình đăng kí"
      );
    }
    delete newUser.payload.userPassword;
    return returnToUser.success(res, "Đăng kí thành công", newUser.payload);
  } catch (error) {
    console.log("register users controller: ", error.message);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = registerUsersController;
