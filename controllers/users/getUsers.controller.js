const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getUsersController = async (req, res) => {
  try {
    let usersDAO = new DAO("users");
    let user = await usersDAO.getById(req.userId);
    if (!user.success)
      return returnToUser.errorWithMess(res, "Lỗi ko tìm thầy users");
    delete user.payload.userPassword;
    returnToUser.success(res, "Thông tin người dùng", user.payload);
  } catch (error) {
    console.log("getUsers controller: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = getUsersController;
