const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const updateUsersController = async (req, res) => {
  try {
    let usersDAO = new DAO("users");
    let user = await usersDAO.updateById(req.userId, req.body);
    if (!user.success)
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi khi cập nhật người dùng"
      );
    delete user.payload.userPassword;
    returnToUser.success(res, "cập nhật thành công", user.payload);
  } catch (error) {
    console.log("update users controller", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = updateUsersController;
