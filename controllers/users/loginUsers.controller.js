const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const jwt = require("../../services/jwt");

const loginUsersController = async (req, res) => {
  try {
    returnToUser.success(res, "Đăng nhập thành công", {
      accessToken: jwt.generateAccessToken({ userId: req.userId }),
      refreshToken: jwt.generateRefreshToken({ userId: req.userId }),
    });
  } catch (error) {
    console.log("login users controller ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = loginUsersController;
