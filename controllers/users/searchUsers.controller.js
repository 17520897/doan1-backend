const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const _ = require("lodash");
const searchUsersController = async (req, res) => {
  try {
    let { userName } = req.body;
    let { page, limit } = req.query;
    let usersDAO = new DAO("users");
    let friendRequestDAO = new DAO("friendRequest");
    let friendPendingDAO = new DAO("friendPending");
    let friendsDAO = new DAO("friends");

    if (!page || !limit) {
      page = 0;
      limit = 5;
    }

    let regex = "";
    for (item of userName) {
      regex += "(?:" + item.toLowerCase() + "|" + item.toUpperCase() + ")";
    }
    regex = `.*(${regex}).*`;

    let users = await usersDAO.getPage(
      { userName: new RegExp(regex), _id: { $ne: req.userId }, isActive: true },
      page,
      limit
    );
    let quantity = users.payload.pop();

    let userLength = users.payload.length;

    for (let i = 0; i < userLength; i++) {
      users.payload[i] = users.payload[i].toObject();
      delete users.payload[i].userPassword;
      delete users.payload[i].userEmail;
      users.payload[i].friendStatus = "normal";
      //check is request
      let check = await friendRequestDAO.getOne({
        userId: req.userId,
        "friendRequests.userRequestId": users.payload[i]._id,
      });
      if (check.success) {
        users.payload[i].friendStatus = "request";
        continue;
      }
      //check is pending
      check = await friendPendingDAO.getOne({
        userId: req.userId,
        "friendPending.userPendingId": users.payload[i]._id,
      });
      if (check.success) {
        users.payload[i].friendStatus = "pending";
        continue;
      }

      //check is friends
      check = await friendsDAO.getOne({
        userId: req.userId,
        "friends.friendId": users.payload[i]._id,
      });
      if (check.success) {
        users.payload[i].friendStatus = "friend";
        continue;
      }
    }

    users.payload[users.payload.length] = quantity;
    returnToUser.success(res, "search", users.payload);
  } catch (error) {
    console.log("searchUsersController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = searchUsersController;
