const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const uploadFileController = async (req, res) => {
  try {
    let { filename } = req.file;
    if (filename === "null")
      return returnToUser.errorWithMess(res, "Upload file không hợp lệ");
    return returnToUser.success(res, {
      url: `${process.env.SERVER_URL}/${process.env.UPLOAD_FOLDER}/${filename}`,
    });
  } catch (err) {
    console.log("uploadFileController: ", err);
    returnToUser.errorProcess(res, err);
  }
};

module.exports = uploadFileController;
