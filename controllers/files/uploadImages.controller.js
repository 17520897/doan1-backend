const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const uploadImagesController = async (req, res) => {
  try {
    let { filename } = req.file;
    if (filename === "null")
      return returnToUser.errorWithMess(res, "Upload file không hợp lệ");
    return returnToUser.success(res, {
      url: `${process.env.SERVER_URL}/${process.env.UPLOAD_FOLDER}/${filename}`,
    });
  } catch (err) {
    console.log("uploadImagesController: ", err);
    returnToUser.errorProcess(res, err);
  }
};

module.exports = uploadImagesController;
