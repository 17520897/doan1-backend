const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const seenRoomMessageController = async (req, res) => {
  try {
    const { roomId } = req.query;
    const roomDAO = new DAO("rooms");
    let room = await roomDAO.updateMany(
      { roomId, "messages.seen": false },
      { "messages.$[].seen": true }
    );
    returnToUser.success(res, "seen message success");
  } catch (error) {
    console.log("seenRoomMessageController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = seenRoomMessageController;
