const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getRoomImagesController = async (req, res) => {
  try {
    const { roomId } = req.query;
    const roomDAO = new DAO("rooms");
    const room = await roomDAO.getOne({ roomId, "messages.type": "image" });
    if (room.success) {
      returnToUser.success(res, "Danh sách hình ảnh", room.payload);
    } else {
      returnToUser.errorWithMess(res, "Chưa có hỉnh ảnh nào");
    }
  } catch (error) {
    console.log("getRoomImagesController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = getRoomImagesController;
