const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getRoomMessagesController = async (req, res) => {
  try {
    const { roomId } = req.query;
    const roomDAO = new DAO("rooms");
    const room = await roomDAO.getOne({ roomId });
    console.log(room);
    if (room.success) {
      returnToUser.success(res, "Danh sách tin nhắn", room.payload);
    } else {
      returnToUser.errorWithMess(res, "Chưa có tin nhắn nào");
    }
  } catch (error) {
    console.log("getRoomMessagesController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = getRoomMessagesController;
