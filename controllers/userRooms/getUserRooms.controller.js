const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getUserRoomsController = async (req, res) => {
  try {
    const userId = req.userId;
    const userRoomDAO = new DAO("userRooms");
    const userDAO = new DAO("users");
    const roomDAO = new DAO("rooms");
    let returnData = [];
    let userRoom = await userRoomDAO.getOne({ userId });
    if (!userRoom.success)
      return returnToUser.errorWithMess(res, "Chưa có phòng chat nào");
    let roomLength = userRoom.payload.rooms.length;
    for (let i = 0; i < roomLength; i++) {
      let user = await userDAO.getById(userRoom.payload.rooms[i].userConnectId);
      let room = await roomDAO.getOne({
        roomId: userRoom.payload.rooms[i].roomId,
      });
      let lastMessage;
      if (room.success && user.success) {
        lastMessage = room.payload.messages[room.payload.messages.length - 1];
        returnData.push({
          connectId: userRoom.payload.rooms[i].userConnectId,
          connectName: user.payload.userName,
          connectAvatar: user.payload.userAvatar,
          roomId: userRoom.payload.rooms[i].roomId,
          lastMessage,
        });
      }
    }
    return returnToUser.success(res, "user room list", returnData);
  } catch (err) {
    console.log("getUserRoomsController: ", err);
    returnToUser.errorProcess(res, err);
  }
};

module.exports = getUserRoomsController;
