const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const mail = require("../../services/mail");

const createNewVerifyController = async (req, res) => {
  try {
    //define
    let configDAO = new DAO("config");
    let verifyDAO = new DAO("verify");
    //make code
    let verifyCode = Date.now().toString(16);
    let { userId, verifyType, userEmail } = req.verify;
    let newVerify = await verifyDAO.create({
      verifyUserId: userId,
      verifyType,
      verifyCode,
    });
    if (!newVerify.success) {
      console.log("create NewVerifyController: ", newVerify.err);
      return returnToUser.errorWithMess(res, "Xảy ra lỗi quá trình tạo mã");
    }
    //get config template mail
    let config = await configDAO.getOne({});
    if (!config.success) {
      console.log("create NewVerifyController: ", newVerify.err);

      return returnToUser.errorWithMess(res, "Xảy ra lỗi quá trình tạo mã");
    }
    //send mail
    let subject = "[Đồ án 1 - Chat app] Mã xác nhận quên mật khẩu";
    let template = config.payload.configVerifyEmailTemplate.replace(
      "{code}",
      verifyCode.toString()
    );
    let sendEmail = await mail.sendMail(userEmail, subject, template);
    if (!sendEmail.success) {
      console.log("create NewVerifyController: ", sendEmail.err);
      return returnToUser.errorWithMess(res, "Xảy ra lỗi quá trình tạo mã");
    }

    //return
    returnToUser.success(res, "Tạo mã xác thực thành công", req.verify);
  } catch (error) {
    console.log("create NewVerifyController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = createNewVerifyController;
