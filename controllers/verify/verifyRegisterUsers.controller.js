const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const mail = require("../../services/mail");

const verifyUsersController = async (req, res) => {
  try {
    //define
    let usersDAO = new DAO("users");
    let verifyDAO = new DAO("verify");
    //active user
    let user = await usersDAO.updateById(req.userId, { isActive: true });
    if (!user.success) {
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi trong quá trình xác thực vui lòng xác thực lại"
      );
    }
    //delete verify;
    await verifyDAO.deleteMany({ verifyUserId: req.userId });
    //return
    returnToUser.success(res, "Xác thực tài khoản thành công", {
      ...user.payload,
      verifyType: "Register",
    });
  } catch (error) {
    console.log("verifyUsersController: ", error);
    returnToUser.erorProcess(res, error);
  }
};

module.exports = verifyUsersController;
