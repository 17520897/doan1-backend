const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const bcrypt = require("bcryptjs");

const verifyForgotPasswordController = async (req, res) => {
  try {
    //define
    let { password } = req.body;
    let usersDAO = new DAO("users");
    let verifyDAO = new DAO("verify");
    //hash password
    password = await bcrypt.hash(password, 10);
    //update user password
    let user = await usersDAO.updateById(req.userId, {
      userPassword: password,
    });
    if (!user.success)
      return returnToUser.errorWithMess(
        res,
        "Xảy ra lỗi trong quá trình đổi mật khẩu - xác thực lại"
      );
    await verifyDAO.deleteMany({ verifyUserId: req.userId });
    delete user.payload.userPassword;
    return returnToUser.success(
      res,
      "Thay đổi mật khẩu thành công",
      user.payload
    );
  } catch (error) {
    console.log("verifyForgotPasswordController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = verifyForgotPasswordController;
