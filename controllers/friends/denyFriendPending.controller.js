const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const notifyConst = require("../../socket/notifyConst");

const denyFriendPendingController = async (req, res) => {
  try {
    const friendPendingDAO = new DAO("friendPending");
    const friendRequestDAO = new DAO("friendRequest");
    const { userPendingId } = req.body;
    const userId = req.userId;
    let friendPending = await friendPendingDAO.getOne({ userId });
    if (!friendPending.success)
      return returnToUser.errorWithMess(
        res,
        "Không tìm thấy dữ liệu người dùng"
      );
    let friendRequest = await friendRequestDAO.getOne({
      userId: userPendingId,
    });
    if (!friendRequest.success)
      return returnToUser.errorWithMess(
        res,
        "Không tìm thấy dữ liệu người mời"
      );
    await friendPendingDAO.updateById(friendPending.payload._id, {
      $pull: {
        friendPending: {
          userPendingId,
        },
      },
    });
    await friendRequestDAO.updateById(friendRequest.payload._id, {
      $pull: {
        friendRequests: {
          userRequestId: userId,
        },
      },
    });
    return returnToUser.successWithNoData(res, "Từ chối kết bạn thành công");
  } catch (error) {
    console.log("deny friend pending controller: ", error);
    return returnToUser.errorProcess(res, error);
  }
};

module.exports = denyFriendPendingController;
