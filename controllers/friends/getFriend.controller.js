const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getFriendController = async (req, res) => {
  try {
    let friendDAO = new DAO("friends");
    let usersDAO = new DAO("users");

    let friend = await friendDAO.getOne({ userId: req.userId });
    if (!friend.success)
      return returnToUser.errorWithMess(res, "Chưa có bạn bè nào");
    let friendLength = friend.payload.friends.length;
    for (let i = 0; i < friendLength; i++) {
      let users = await usersDAO.getById(friend.payload.friends[i].friendId);
      if (users.success) {
        users = users.payload.toObject();
        friend.payload.friends[i] = {
          userName: users.userName,
          userFriendId: users._id,
          userAvatar: users.userAvatar,
          userGender: users.userGender,
          acceptTime: friend.payload.friends[i].acceptTime,
        };
      }
    }
    returnToUser.success(res, "Danh sách bạn bè", friend.payload.friends);
  } catch (error) {
    console.log("getFriendController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = getFriendController;
