const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");
const notifyConst = require("../../socket/notifyConst");
const acceptFriendPendingController = async (req, res) => {
  try {
    let { userPendingId } = req.body;
    let friendPendingDAO = new DAO("friendPending");
    let friendRequestDAO = new DAO("friendRequest");
    let friendDAO = new DAO("friends");
    const userDAO = new DAO("users");
    const notifyDAO = new DAO("notify");
    let friendPending = await friendPendingDAO.getOne({
      userId: req.userId,
      "friendPending.userPendingId": userPendingId,
    });
    if (!friendPending.success)
      return returnToUser.errorWithMess(
        res,
        "Không có lời mời kết bạn này để accept"
      );

    let friend = await friendDAO.getOne({
      userId: req.userId,
    });
    if (friend.success) {
      friend = await friendDAO.updateOne(
        { userId: req.userId },
        {
          $push: {
            friends: {
              friendId: userPendingId,
            },
          },
        },
        {
          new: true,
        }
      );
    } else {
      friend = await friendDAO.create({
        userId: req.userId,
        friends: [
          {
            friendId: userPendingId,
          },
        ],
      });
    }
    let acceptFriend = await friendDAO.getOne({ userId: userPendingId });
    if (acceptFriend.success) {
      acceptFriend = await friendDAO.updateOne(
        { userId: userPendingId },
        {
          $push: {
            friends: {
              friendId: req.userId,
            },
          },
        },
        {
          new: true,
        }
      );
    } else {
      acceptFriend = await friendDAO.create({
        userId: userPendingId,
        friends: [
          {
            friendId: req.userId,
          },
        ],
      });
    }
    await friendPendingDAO.updateOne(
      { userId: req.userId },
      {
        $pull: {
          friendPending: {
            userPendingId,
          },
        },
      }
    );
    //update cho người chấp thuận
    await friendRequestDAO.updateOne(
      {
        userId: req.userId,
      },
      {
        $pull: {
          friendRequests: {
            userRequestId: userPendingId,
          },
        },
      }
    );
    //update cho người gửi
    await friendPendingDAO.updateOne(
      { userId: userPendingId },
      {
        $pull: {
          friendPending: {
            userPendingId: req.userId,
          },
        },
      }
    );
    await friendRequestDAO.updateOne(
      {
        userId: userPendingId,
      },
      {
        $pull: {
          friendRequests: {
            userRequestId: req.userId,
          },
        },
      }
    );
    const user = await userDAO.getById(req.userId);
    if (user.success) {
      await notifyDAO.create({
        userId: userPendingId,
        sendingId: req.userId,
        type: "acceptFriend",
        notify: user.payload.userName + notifyConst.acceptFriend,
      });
    }
    returnToUser.successWithNoData(res, "Kết bạn thành công");
  } catch (error) {
    console.log("acceptFriendPendingController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = acceptFriendPendingController;
