const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const getUsersNotifyController = async (req, res) => {
  try {
    let userId = req.userId;
    const notifyDAO = await new DAO("notify");
    const notify = await notifyDAO.getByQuery({ userId });
    if (notify.success)
      return returnToUser.success(res, "get notify success", notify.payload);
    else return returnToUser.errorWithMess(res, notify.err);
  } catch (error) {
    console.log("getUsersNotifyController: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = getUsersNotifyController;
