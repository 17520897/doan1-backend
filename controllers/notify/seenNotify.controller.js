const DAO = require("../../classes/DAO");
const returnToUser = require("../../services/returnToUser");

const seenNotifyController = async (req, res) => {
  try {
    const notifyDAO = new DAO("notify");

    await notifyDAO.updateMany({ userId: req.userId }, { seen: true });
    return returnToUser.success(res, "Seen notify success");
  } catch (error) {
    console.log("seen notify controller: ", error);
    returnToUser.errorProcess(res, error);
  }
};

module.exports = seenNotifyController;
