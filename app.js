var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var app = express();
const { PeerServer } = require("peer");

const peerServer = PeerServer({ port: 3541, path: "/webRtc" });

const cors = require("cors");

//  apply to all requests

require("dotenv").config();
require("./model/connect");
require("./model/schema");

app.use(cors());
// view engine setup
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.get("/socket", (req, res) => {
  res.render("test.ejs");
});
app.get("/webrtc", (req, res) => {
  res.render("webRtc.ejs");
});
app.get("/video", (req, res) => {
  res.render("call.ejs");
});
app.use("/", require("./routes"));

module.exports = app;
